function showDeleteModal(obj) {
    $('#deleteDBname').text(obj.dataset.name);
    $('#deleteDBinput').data('should',obj.dataset.name);
    $('#deleteModal').data('cid',obj.dataset.cid);
    $('#deleteModal').modal('show');
}

function checkDBName(obj) {
    if($('#deleteDBinput').val() === $(obj).data('should')) {
        $('#deleteButton').prop('disabled',false);
        $('#deleteGroup').removeClass('has-error');
        $('#deleteGroup .help-block').addClass('hidden');
    }
    else {
        $('#deleteButton').prop('disabled',true);
        $('#deleteGroup').addClass('has-error');
        $('#deleteGroup .help-block').removeClass('hidden');
    }
}
