var editor;
var gcid = '';
var gtid;
var activeQuestion;
$(document).ready(function() {
    ace.require("ace/ext/language_tools");
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/sqlserver");
    editor.getSession().setMode("ace/mode/sql");
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true
    });
    editor.setFontSize(14);
    editor.$blockScrolling = Infinity;

    $('#submitBtn').click(submitClick);
});

function submitClick(){
    $('#submitSpinner').toggleClass('hidden');
    $('.results').toggleClass('hidden');
    $('#submitBtn').off('click');
    $.post('/api/databases/query',{
        cid: $('#editor').attr('data-cid'),
        query: editor.getValue(),
        qid: activeQuestion,
        tid: gtid
    }).done((data) => {
        $('#submitSpinner').toggleClass('hidden');
        $('.results').toggleClass('hidden');
        $('#submitBtn').click(submitClick);

        $('#countAlert').remove();
        $('#errAlert').remove();
        $('#columns').empty();
        $('#data').empty();

        if(data.Message) {
            $('.alerts').append($('<div>',{
                class: 'alert alert-danger alert-dismissable',
                id:'errAlert'
            }));
            var errAlert = $('#errAlert');
            errAlert.append($('<button>',{
                type:'button',
                class:'close',
                'data-dismiss': 'alert',
                'aria-hidden':'true',
                id:'cbtn'
            }));
            $('#cbtn').append($('<span/>',{class:'pficon pficon-close'}));
            errAlert.append($('<span/>',{class:'pficon pficon-error-circle-o'}));
            errAlert.append($('<span/>',{text:data.Message}));
            matchHeight();
            return;
        }

        var d = new Date();
        $("#diagram").attr("data", "/diagram/"+$('#editor').attr('data-cid')+"?"+d.getTime());

        var rowcount = data.Results ? data.Results.length : 0;
        $('.alerts').append($('<div>',{
            class: 'alert alert-success',
            id:'countAlert'
        }));
        var countAlert = $('#countAlert');
        countAlert.append($('<span/>',{class:'pficon pficon-ok'}));
        countAlert.append($('<span/>',{text:'Your query returned: '+rowcount+' Rows'}));
        if(data.Columns == null){
            matchHeight();
            return;
        }

        data.Columns.forEach((name) => {
            $('#columns').append($('<th>'+name+'</th>'));
        });
        data.Results.forEach((res,i) => {
            $('#data').append($('<tr>',{id:'row-'+i}));
            var row = $('#row-'+i);
            res.forEach((val) => {
                row.append($('<td>'+val+'</td>'));
            });
        });
        matchHeight();
    });
}
