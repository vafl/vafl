var editor;
$(document).ready(function () {
    // click the list-view heading then expand a row
    $(".list-group-item-header").click(function(event){
        if(!$(event.target).is("button, a, input, .fa-ellipsis-v")){
            $(this).find(".fa-angle-right").toggleClass("fa-angle-down")
                .end().parent().toggleClass("list-view-pf-expand-active")
                .find(".list-group-item-container").toggleClass("hidden");
        } else {
        }
    });

    $('.selectpicker').selectpicker();
    $.get('/api/databases').then((data) => {
        data.forEach((el) => {
            $('#qpschemapicker').append($('<option>', {
                value: el.Id,
                text: el.Name
            }));
            $('#qpschemapicker').selectpicker('refresh');
        });
    });
    // click the close button, hide the expand row and remove the active status
    $(".list-group-item-container .close").on("click", function (){
        $(this).parent().addClass("hidden")
            .parent().removeClass("list-view-pf-expand-active")
            .find(".fa-angle-right").removeClass("fa-angle-down");
    });
    prettyPrint();

    $('#addQPButton').click(() => {
        $.ajax({
            url: '/api/questionpools',
            type: 'POST',
            data: {
                name: $('input[name="qpname"]').val(),
                schema: $('#qpschemapicker').val()
            },
            success: () => {
                window.location.reload();
            },
            error: (xhr) => {
                alert('Unable to add questionpool - Please try again later');
            }
        });
    });

    ace.require("ace/ext/language_tools");
    editor = ace.edit("questionEditor");
    editor.setTheme("ace/theme/sqlserver");
    editor.getSession().setMode("ace/mode/sql");
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true
    });
    editor.setFontSize(14);
    editor.$blockScrolling = Infinity;

});

function editQuestion(target) {
    var qid = target.dataset.qid;
    $('#questionEditorWrapper').addClass('hidden');
    $('#questionEditorSpinner').removeClass('hidden');
    editor.setValue('');
    $('#questionEditorModal').modal('show');
    $.get('/api/questions/'+qid).done((data) => {
        $('#questionEditorSpinner').addClass('hidden');
        $('#questionEditorWrapper').removeClass('hidden');
        $('#textInput-question-editor').val(data.Content);
        $('#numberInput-question-points').val(data.Points);
        editor.setValue(data.Query);
        $('#saveButton').off('click');
        $('#saveButton').click(() => {
            var cnt = $('#textInput-question-editor').val();
            var qry = editor.getValue();
            var points = $('#numberInput-question-points').val();
            $('#questionEditorWrapper').addClass('hidden');
            $('#questionEditorSpinner').removeClass('hidden');
            $.ajax({
                url: '/api/questions/'+qid,
                type: 'PUT',
                data: {
                    content: cnt,
                    query: qry,
                    points: points
                },
                success: () => {
                    $('#qcontent-'+qid).text(cnt);
                    $('#qquery-'+qid).removeClass('prettyprinted');
                    $('#qquery-'+qid).html(qry);
                    prettyPrint();
                    $('#questionEditorModal').modal('hide');
                },
                error: (xhr) => {
                    $('#questionEditorSpinner').addClass('hidden');
                    $('#questionEditorWrapper').removeClass('hidden');
                    console.log(xhr);
                }
            });
        });
    }).fail(() => console.log('Error getting question'));
}

function createQobject(id,poolid) {
    var item = $.parseHTML(`
<div class="panel panel-default" id="qpanel-${id}">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a aria-expanded="false" class="collapsed" data-no-instant="" data-parent="#accordion-pool-${poolid}" data-toggle="collapse" href="#collapse-${id}" id="qcontent-${id}"></a>
        </h4>
    </div>
</div>
     `);
    var expand = $.parseHTML(`
<div aria-expanded="false" class="panel-collapse collapse" id="collapse-${id}">
    <div class="panel-body">
        <pre style="" class="prettyprint" id="qquery-${id}"></pre>
        <button class="btn btn-default" data-no-instant="" data-qid="${id}" onclick="editQuestion(this)" type="button">Edit Question</button>
        <button class="btn btn-danger" data-no-instant="" data-qid="${id}" onclick="deleteQuestion(this)" type="button">Delete Question</button>
    </div>
</div>
`);
    $(`#accordion-pool-${poolid}`).append(item);
    $(`#accordion-pool-${poolid}`).append(expand);

}

function addQuestion(target) {
    var pid = target.dataset.pid;
    editor.setValue('');
    $('#textInput-question-editor').val('');
    $('#numberInput-question-points').val(0);
    $('#questionEditorModal').modal('show');
    $('#questionEditorSpinner').addClass('hidden');
    $('#questionEditorWrapper').removeClass('hidden');
    editor.setValue('');
    $('#saveButton').off('click');
    $('#saveButton').click(() => {
        var cnt = $('#textInput-question-editor').val();
        var qry = editor.getValue();
        var points = $('#numberInput-question-points').val();
        $.ajax({
            url: '/api/questions',
            type: 'POST',
            data: {
                content: cnt,
                query: qry,
                points: points,
                pool: pid
            },
            success: (data) => {
                var qid = data.Id;
                createQobject(data.Id,pid);
                $('#qcontent-'+qid).text(cnt);
                $('#qquery-'+qid).removeClass('prettyprinted');
                $('#qquery-'+qid).html(qry);
                prettyPrint();
                $('#questionEditorModal').modal('hide');
            },
            error: (xhr) => {
                console.log(xhr);
            }
        });
    });
}

function deleteQuestion(target) {
    var qid = target.dataset.qid;
    $.ajax({
        url: '/api/questions/'+qid,
        type: 'DELETE',
        success: () => {
            $('#qpanel-'+qid).remove();
            $('#collapse-'+qid).remove();
        }
    });
}
