$(document).ready(function() {
    $('#start-date').datetimepicker({
        format : 'DD/MM/YYYY HH:mm'
    });
    $('#end-date').datetimepicker({
        format : 'DD/MM/YYYY HH:mm'
    });
    $.get('/api/questionpools').then((data) => {
        data.forEach((el) => {
            var elem = $('<option>'+el.Name+'</option>');
            $('#poolpicker').append($('<option>', {
                value: el.Id,
                text: el.Name
            }));
            $('#poolpicker').selectpicker('refresh');
        });
    });
});
function addTest() {
    var slist = [];
    $('input[type="checkbox"]:checked').each((k,el) =>{
        slist.push(el.name);
    });
    if(slist.length <= 0){
        alert('You must select at least one student');
    }
    $.post('/api/tests',{
        pool: $('#poolpicker').val(),
        students: JSON.stringify(slist),
        name: $('#textInput-test-wizard').val(),
        start: $('#start-date').data("DateTimePicker").date().utc().toISOString(),
        end: $('#end-date').data("DateTimePicker").date().utc().toISOString()
    }).done(() => {
        window.location.replace('/');
    });
}
