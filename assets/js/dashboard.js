$(document).ready(function() {
    matchHeight();
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);
    $('.selectpicker').selectpicker();
    $.get('/api/databases').then((data) => {
        data.forEach((el) => {
            var elem = $('<option>'+el.Name+'</option>');
            $('#schemapicker').append($('<option>', {
                value: el.Id,
                text: el.Name
            }));
            $('#schemapicker').selectpicker('refresh');
        });
    });
    $('#deployButton').click(() => {
        $('#deploySpinner').toggleClass('hidden');
        $.post('/api/containers',{
            name: $('#wizardModal input[name="dbname"]').val(),
            dbid: $('select[name="dbschema"]').val()
        }).done(() => {
            window.location.reload();
        }).fail((xhr) => {
            $('#deploySpinner').toggleClass('hidden');
            $('#wizardError').removeClass('hidden');
            $('#wizardErrorMsg').text(xhr.responseJSON.Message);
        });
    });
    $('#deleteButton').click(() => {
        $('#deleteSpinner').toggleClass('hidden');
        $.ajax({
            url: '/api/containers/' + $('#deleteModal').data('cid'),
            type: 'DELETE'
        }).done(() => {
            window.location.reload();
        }).fail((xhr) => console.log(xhr));
    });
    $('#uploadform').submit(function(e){
        e.preventDefault();
        $.ajax({
            url:'/api/upload',
            type:'post',
            data: new FormData($('form')[2]),
            success:function(){
                window.location.reload();
            },
            error:function(xhr){
                $('#uploadError').removeClass('hidden');
                $('#uploadErrorMsg').text(xhr.responseJSON.Message);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $('#uploadButton').click(() => {
        $('#uploadform').submit();
    });
});
