var questions = [];
var submissions = [];
$(document).ready(function() {
    $('#questionnavigation .qnum').each((idx,el) => {
        $(el).text(idx+1);
    });
    gtid = $('#questionnavigation').data().tid;
    initializeQuestions(gtid).then(() => {
        switchQuestion($('#questionnavigation .qnum')[0]);
    });
    gcid = $('#editor').data().cid;
});

function switchQuestion(el) {
    var qid = el.dataset.qid;
    $('#questionnavigation .qitem').each((idx,el) => $(el).removeClass('active'));
    $(el).parent().addClass('active');;
    $('#qcontent').text(questions[qid].Content);
    getResult(gcid,qid);
    activeQuestion = qid;
}

function initializeQuestions(tid) {
    return new Promise((resolve) => {
        $.get(`/api/testquestions/${tid}`).done((data) => {
            questions = [];
            data.forEach((el) => {
                questions[el.Id] = el;
            });
            resolve();
        });
    });
}

function getResult(cid,qid) {
    $('#expcolumns').empty();
    $('#expdata').empty();
    $.get(`/api/expectedresults/${cid}?qid=${qid}`).done((data) => {
        data.Columns.forEach((name) => {
            $('#expcolumns').append($('<th>'+name+'</th>'));
        });
        data.Results.forEach((res,i) => {
            $('#expdata').append($('<tr>',{id:'exprow-'+i}));
            var row = $('#exprow-'+i);
            res.forEach((val) => {
                row.append($('<td>'+val+'</td>'));
            });
        });
        matchHeight();
    });
}

function checkClick() {
    checkQuestion(activeQuestion,editor.getValue(),gcid);
}

function checkQuestion(qid,query,cid) {
    submissions[qid] = query;
    $(`#questionnavigation`)[0].scrollIntoView({behavior:'smooth',block:'end'});
    $.post(`/api/checkquestion/${qid}`,{
        cid: cid,
        query: query
    }).done(() => {
        $(`a[data-qid=${qid}]`).parent().removeClass('wrong');
        $(`a[data-qid=${qid}]`).parent().addClass('completed');
    }).fail(() => {
        $(`a[data-qid=${qid}]`).parent().removeClass('completed');
        $(`a[data-qid=${qid}]`).parent().addClass('wrong');
    });
}

function submitTest() {
    var smb = [];
    submissions.forEach((query,id) => {
        smb.push({
            Id:id,
            Query:query
        });
    });
    $.ajax({
        type:'POST',
        url:`/api/tests/${gtid}/scores?cid=${gcid}`,
        data: JSON.stringify(smb)
    }).done(() => window.location.replace('/'));
}
