package core

import (
	"database/sql"

	"github.com/docker/docker/client"
)

type Vafl struct {
	Database *sql.DB
	ClientID string
	Docker   *client.Client
}

// GetUser retrieves a functioning user object from the database and initializes the Name
// by querying the classroom api
func (v *Vafl) GetUser(atkn string) (*User, error) {
	u := User{AccessToken: atkn}
	u.InitializeService()

	cu, err := u.srv.UserProfiles.Get("me").Do()
	if err != nil {
		return nil, err
	}

	u.Id = cu.Id
	u.FirstName = cu.Name.GivenName
	u.LastName = cu.Name.FamilyName
	u.Vafl = v
	return &u, nil
}

// UpsertUser either inserts or updates a user record in the database.
// It does only update the access token as other variables are supplied by the classroom api
func (v *Vafl) UpsertUser(u *User) error {
	_, err := v.Database.Exec("INSERT INTO users (id,token) VALUES ($1,$2) ON CONFLICT(id) DO UPDATE SET token = $2", u.Id, u.AccessToken)
	if err != nil {
		return err
	}
	return nil
}
