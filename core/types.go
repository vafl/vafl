package core

import (
	"github.com/docker/docker/api/types"
	"time"
)

type Test struct {
	Id    int
	Name  string
	Pool  Questionpool
	Start time.Time
	End   time.Time
}

type Questionpool struct {
	Id        int
	Name      string
	Dbid      int
	Questions []Question
}

type Question struct {
	Id      int
	Content string
	Query   string
	User    User
	Points  int
}

type Course struct {
	Name     string
	Students []User
}

type TestAttempt struct {
	Test      Test
	Container string
}

type TestQuery struct {
	Query string
	Final bool
}

type QuestionResult struct {
	Question Question
	Results  map[*User][]TestQuery
}

type TestResult struct {
	Test    Test
	Results []QuestionResult
}

type MetaTest struct {
	Id        int
	Name      string
	Submitted int
}

type UserDB struct {
	Name       string
	SchemaName string
	Container  types.ContainerJSON
}

type DatabaseSchema struct {
	Id   int
	Name string
	Path string
}
