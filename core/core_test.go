package core

import (
	"database/sql"
	"testing"

	"github.com/BurntSushi/toml"
	docker "github.com/docker/docker/client"
	_ "github.com/lib/pq"

	c "gitlab.com/vafl/vafl/configuration"
)

var testUser = User{
	Id:          "1234",
	AccessToken: "token",
}

func GetTestEnvironment() (*Vafl, error) {
	var settings c.Configuration
	_, err := toml.DecodeFile("../testdata.toml", &settings)
	if err != nil {
		return nil, err
	}

	db, err := sql.Open("postgres", settings.Database.ConnectionString())
	if err != nil {
		return nil, err
	}

	dcl, err := docker.NewEnvClient()
	if err != nil {
		return nil, err
	}

	v := &Vafl{
		Database: db,
		ClientID: settings.Google.ClientID,
		Docker:   dcl,
	}
	v.Cleanup()
	v.PrepareTest()
	return v, nil
}

func (v *Vafl) PrepareTest() {
	// Preparing the databse to comply with constraints
	v.Database.Exec("INSERT INTO schemas (id,name,path) VALUES (1337,'name1','/tmp/schema.sql')")
	v.Database.Exec("INSERT INTO questionpools (id,name,dbid) VALUES (1,'testpool',1337)")
	v.Database.Exec("INSERT INTO users (id,token) VALUES (42,'')")
	v.Database.Exec("INSERT INTO users (id,token) VALUES (43,'')")
}

func (v *Vafl) Cleanup() {
	// Clean up tables used by tests
	v.Database.Exec("DELETE FROM containers")
	v.Database.Exec("DELETE FROM users")
	v.Database.Exec("DELETE FROM schemas")
	v.Database.Exec("DELETE FROM testattempt")
	v.Database.Exec("DELETE FROM tests")
}

func TestUpserUser(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	err = env.UpsertUser(&testUser)
	if err != nil {
		t.Error(err)
	}
	var token string
	err = env.Database.QueryRow("SELECT token FROM users WHERE id=$1", testUser.Id).Scan(&token)
	if err != nil {
		t.Error(err)
	}
	if token != testUser.AccessToken {
		t.Errorf("Inserting User Failed: %s != %s", token, testUser.AccessToken)
	}
	testUser.AccessToken = "newToken" //Updating token
	err = env.UpsertUser(&testUser)
	if err != nil {
		t.Error(err)
	}

	err = env.Database.QueryRow("SELECT token FROM users WHERE id=$1", testUser.Id).Scan(&token)
	if err != nil {
		t.Error(err)
	}
	if token != testUser.AccessToken {
		t.Errorf("Updating User Failed: %s != %s", token, testUser.AccessToken)
	}
}
