package core

import (
	"fmt"
	"time"
)

func (v *Vafl) CreateTest(name string, pool int, start time.Time, end time.Time, ids []string) error {
	qpool, err := v.GetQuestionpool(pool)
	if err != nil {
		return err
	}

	tr, err := v.Database.Begin()
	if err != nil {
		return err
	}

	var tid int
	err = tr.QueryRow(`INSERT INTO tests (name,qpid,start,"end") VALUES ($1,$2,$3,$4) RETURNING id`, name, pool, start, end).Scan(&tid)
	if err != nil {
		tr.Rollback()
		return err
	}

	for _, s := range ids {
		v.Database.Exec(`INSERT INTO users (id,token) VALUES ($1,'0') ON CONFLICT DO NOTHING`, s)
		cnt, err := v.NewUserDB(s, qpool.Dbid, fmt.Sprintf("test-%d-%s", tid, s))
		if err != nil {
			tr.Rollback()
			return err
		}
		_, err = tr.Exec(`INSERT INTO testattempt (uid,tid,cid) VALUES ($1,$2,$3)`, s, tid, cnt.ID)
		if err != nil {
			tr.Rollback()
			return err
		}
	}

	err = tr.Commit()
	return err
}

func (v *Vafl) GetTestAttempt(u *User, tid int) (TestAttempt, error) {
	ta := TestAttempt{}
	var qp int
	err := v.Database.QueryRow(`SELECT tests.id,tests.Name,testattempt.cid,qpid,tests.start,tests.end
                         FROM testattempt
                         JOIN tests ON tid=tests.id WHERE testattempt.uid = $1 AND testattempt.tid = $2`,
		u.Id, tid).Scan(&ta.Test.Id, &ta.Test.Name, &ta.Container, &qp, &ta.Test.Start, &ta.Test.End)
	if err != nil {
		return ta, err
	}
	pool, err := v.GetQuestionpool(qp)
	if err != nil {
		return ta, err
	}
	pool.Questions, err = v.GetQuestions(qp)
	if err != nil {
		return ta, err
	}
	ta.Test.Pool = pool
	return ta, nil
}

func (v *Vafl) SubmitTestScore(u *User, tid int, points int) error {
	_, err := v.Database.Exec(`INSERT INTO testscore (sid,tid,date,score) VALUES ($1,$2,now(),$3)`, u.Id, tid, points)
	return err
}

func (v *Vafl) DeleteTestAttempt(u *User, tid int) error {
	var cid string
	err := v.Database.QueryRow(`DELETE FROM testattempt WHERE uid=$1 AND tid=$2 RETURNING cid`, u.Id, tid).Scan(&cid)
	if err != nil {
		return err
	}
	return v.DeleteUserDB(u, cid)
}

func (v *Vafl) GetTestResults(tid int) (TestResult, error) {
	tr := TestResult{}
	t, err := v.GetTest(tid)
	if err != nil {
		return tr, nil
	}
	tr.Test = t

	users, err := v.Database.Query(`SELECT DISTINCT uid,token FROM queries JOIN users ON uid=users.id WHERE tid=$1`, tid)
	if err != nil {
		return tr, err
	}
	userpointers := make(map[string]*User)
	for users.Next() {
		u := User{}
		users.Scan(&u.Id, &u.AccessToken)
		err := u.InitializeFields()
		if err != nil {
			return tr, err
		}
		userpointers[u.Id] = &u
	}

	finalanswers := make(map[int]bool)

	finals, err := v.Database.Query(`SELECT query FROM finalAnswers WHERE tid=$1`, tid)
	if err != nil {
		return tr, err
	}
	for finals.Next() {
		var qid int
		finals.Scan(&qid)
		finalanswers[qid] = true
	}

	res, err := v.Database.Query(`SELECT qid FROM tests JOIN inpool ON pid=qpid WHERE tests.id=$1`, tid)
	if err != nil {
		return tr, err
	}
	for res.Next() {
		q := QuestionResult{}
		q.Results = make(map[*User][]TestQuery)

		var qid int
		err := res.Scan(&qid)
		if err != nil {
			return tr, err
		}

		q.Question, err = v.GetQuestion(qid)
		if err != nil {
			return tr, err
		}

		qres, err := v.Database.Query(`SELECT id,query,uid FROM queries
                                     WHERE qid=$1 AND tid=$2`, q.Question.Id, tid)
		if err != nil {
			return tr, err
		}
		for qres.Next() {
			var uid string
			var query string
			var qryid int
			qres.Scan(&qryid, &query, &uid)
			q.Results[userpointers[uid]] = append(q.Results[userpointers[uid]], TestQuery{Query: query, Final: finalanswers[qryid]})
		}
		tr.Results = append(tr.Results, q)
	}
	return tr, err
}

func (v *Vafl) GetTest(tid int) (Test, error) {
	t := Test{}
	var qpid int
	err := v.Database.QueryRow(`SELECT name,start,"end",qpid FROM tests WHERE tests.id=$1`, tid).Scan(&t.Name, &t.Start, &t.End, &qpid)
	if err != nil {
		return t, err
	}
	pool, err := v.GetQuestionpool(qpid)
	t.Pool = pool
	return t, err
}

func (v *Vafl) GetMetaTests(cid string) ([]MetaTest, error) {
	res, err := v.Database.Query(`SELECT tid,name,count(*) FROM testscore JOIN tests ON tid=tests.id GROUP BY tid,name`)
	if err != nil {
		return nil, err
	}
	var tests []MetaTest
	for res.Next() {
		t := MetaTest{}
		res.Scan(&t.Id, &t.Name, &t.Submitted)
		tests = append(tests, t)
	}

	return tests, nil
}
