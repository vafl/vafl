package core

func (v *Vafl) GetQuestionpools() ([]Questionpool, error) {
	res, err := v.Database.Query(`SELECT id,name,dbid FROM questionpools`)
	if err != nil {
		return nil, err
	}

	var pools []Questionpool
	for res.Next() {
		pool := Questionpool{}
		err := res.Scan(&pool.Id, &pool.Name, &pool.Dbid)
		if err != nil {
			return nil, err
		}
		pools = append(pools, pool)
	}

	for idx, p := range pools {
		res, err := v.Database.Query(`SELECT id,content,query,points FROM inpool JOIN questions ON qid=id WHERE pid = $1 ORDER BY qid`, p.Id)
		if err != nil {
			return nil, err
		}
		var questions []Question
		for res.Next() {
			q := Question{}
			err := res.Scan(&q.Id, &q.Content, &q.Query, &q.Points)
			if err != nil {
				return nil, err
			}
			questions = append(questions, q)
		}
		pools[idx].Questions = questions
	}
	return pools, nil
}

func (v *Vafl) GetQuestionpool(id int) (Questionpool, error) {
	pool := Questionpool{}
	err := v.Database.QueryRow(`SELECT id,name,dbid FROM questionpools WHERE id=$1`, id).Scan(&pool.Id, &pool.Name, &pool.Dbid)
	return pool, err
}

func (v *Vafl) AddQuestionpool(name string, schema int) error {
	_, err := v.Database.Exec(`INSERT INTO questionpools (name,dbid) VALUES ($1,$2)`, name, schema)
	return err
}

func (v *Vafl) GetQuestion(id int) (Question, error) {
	q := Question{Id: id}
	err := v.Database.QueryRow(`SELECT content,query,points FROM questions WHERE id=$1`, id).Scan(&q.Content, &q.Query, &q.Points)
	return q, err
}

func (v *Vafl) UpdateQuestion(q Question) error {
	_, err := v.Database.Exec(`UPDATE questions SET content=$1,query=$2,points=$3 WHERE id=$4`, q.Content, q.Query, q.Points, q.Id)
	return err
}

func (v *Vafl) DeleteQuestion(q int) error {
	_, err := v.Database.Exec(`DELETE FROM questions WHERE id=$1`, q)
	return err
}

func (v *Vafl) AddQuestion(q Question, pid int) (int, error) {
	tr, err := v.Database.Begin()
	if err != nil {
		return 0, err
	}
	err = tr.QueryRow(`INSERT INTO questions (content,query,points,tid)
                      VALUES ($1,$2,$3,$4)
                      RETURNING id `, q.Content, q.Query, q.Points, q.User.Id).Scan(&q.Id)
	if err != nil {
		return 0, err
	}
	_, err = tr.Exec(`INSERT INTO inpool (qid,pid) VALUES($1,$2)`, q.Id, pid)
	if err != nil {
		return 0, err
	}

	err = tr.Commit()
	return q.Id, err
}

func (v *Vafl) GetQuestions(pid int) ([]Question, error) {
	res, err := v.Database.Query(`SELECT id,content,query,points FROM inpool JOIN questions ON qid=id WHERE pid=$1 ORDER BY qid`, pid)
	if err != nil {
		return nil, err
	}
	var questions []Question
	for res.Next() {
		q := Question{}
		err := res.Scan(&q.Id, &q.Content, &q.Query, &q.Points)
		if err != nil {
			return nil, err
		}
		questions = append(questions, q)
	}
	return questions, nil
}
