package core

import (
	"context"
	"github.com/docker/docker/api/types"
	"testing"
	"time"
)

func TestRegisterSchema(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	err = env.RegisterSchema("Testschema", "/tmp/schema")
	if err != nil {
		t.Error(err)
	}
	var path string
	err = env.Database.QueryRow("SELECT path FROM schemas WHERE name=$1 ", "Testschema").Scan(&path)
	if err != nil {
		t.Error(err)
	}
	if path != "/tmp/schema" {
		t.Errorf("Failed to register Schema! %s != %s", path, "/tmp/schema")
	}
	env.Database.Exec("DELETE FROM schemas")
}

func TestGetSchemas(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	schemas, err := env.GetSchemas()
	if err != nil {
		t.Error(err)
	}
	if len(schemas) == 0 {
		t.Error("Failed to get schema")
	}
}

func TestNewUserDB(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	cntJSON, err := env.NewUserDB("42", 1337, "testuserdb")
	if err != nil {
		t.Error(err)
	}
	var cid string
	env.Database.QueryRow("SELECT id FROM containers WHERE name='testuserdb'").Scan(&cid)
	if cntJSON.ID != cid {
		t.Errorf("Failed to insert container. %s != %s", cntJSON.ID, cid)
	}
}

func TestDeleteUserDB(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	cntJSON, err := env.NewUserDB("42", 1337, "testdeletedb")
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	err = env.DeleteUserDB(nil, cntJSON.ID)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	lst, _ := env.Docker.ContainerList(context.Background(), types.ContainerListOptions{})
	deleted := true
	for _, cnt := range lst {
		deleted = deleted && !(cnt.ID == cntJSON.ID)
	}
	if !deleted {
		t.Error("Container still present after deleting")
	}
}

func TestGetUserDBs(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	cntJSON, err := env.NewUserDB("43", 1337, "testdeletedb")
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	dbs, err := env.GetUserDBs(&User{Id: "43"})
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	if len(dbs) != 1 || dbs[0].Container.ID != cntJSON.ID {
		t.Error("Failed to get userdbs")
	}
}

func TestGetUserDB(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	cntJSON, err := env.NewUserDB("42", 1337, "testgetdb")
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	db, err := env.GetUserDB(&User{Id: "42"}, cntJSON.ID)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	if db.Container.ID != cntJSON.ID {
		t.Errorf("Failed to get userdb: %s != %s", db.Container.ID, cntJSON.ID)
	}
	if db.SchemaName != "name1" {
		t.Errorf("Wrong schema name! %s != %s", db.SchemaName, "name1name1")
	}
	_, err = env.GetUserDB(&User{Id: "1"}, cntJSON.ID)
	if err == nil {
		t.Errorf("User was able to access a database which is not his!")
	}
}

func TestQueryUserDB(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	cntJSON, err := env.NewUserDB("42", 1337, "testquerydb")
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	time.Sleep(time.Second * 20)
	_, mysqlerr, err := env.QueryDatabase(&User{Id: "42"}, "SELECT * FROM arbeitet_an", cntJSON.ID, 0, 0, false, false)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	if mysqlerr != nil {
		t.Errorf("Failed to query userdb (mysql error): %s", mysqlerr)
	}
	_, mysqlerr, err = env.QueryDatabase(&User{Id: "42"}, "SELECT bullshit", cntJSON.ID, 0, 0, false, false)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	if mysqlerr.Error() != "Error 1054: Unknown column 'bullshit' in 'field list'" {
		t.Errorf("No error thrown")
	}
}
