package core

import (
	"context"
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	_ "github.com/go-sql-driver/mysql"
)

//NewUserDB creates a new docker container with a specified schema

func (v *Vafl) NewUserDB(uid string, schemaid int, name string) (types.ContainerJSON, error) {
	exists, err := v.DBExists(uid, name)
	if err != nil {
		return types.ContainerJSON{}, errors.New("Error checking preconditions")
	}
	if exists {
		return types.ContainerJSON{}, errors.New("Name already exists")
	}

	// Retrieve the path for the database schema from the database
	var spath string
	err = v.Database.QueryRow("SELECT path FROM schemas WHERE id=$1", schemaid).Scan(&spath)
	if err != nil {
		return types.ContainerJSON{}, err
	}

	conf := &container.Config{
		Image: "mariadb",
		Env:   []string{"MYSQL_ROOT_PASSWORD=root"}, // TODO: Make this more secure!!
	}

	// Mount the SQL file for the specific schema
	hostconf := &container.HostConfig{
		Binds: []string{fmt.Sprintf("%s:%s", spath, "/docker-entrypoint-initdb.d/schema.sql")},
	}
	// Hash the user with the schemaid and dbname to get a unique name
	// TODO: Add current time to hash
	cntname := fmt.Sprintf("vafl-%x", md5.Sum([]byte(uid+strconv.Itoa(schemaid)+name)))

	resp, err := v.Docker.ContainerCreate(context.Background(), conf, hostconf, &network.NetworkingConfig{}, cntname)
	if err != nil {
		return types.ContainerJSON{}, err
	}

	err = v.Docker.ContainerStart(context.Background(), resp.ID, types.ContainerStartOptions{})
	if err != nil {
		return types.ContainerJSON{}, err
	}

	// If the deploy has been successful, store it in the database
	_, err = v.Database.Exec("INSERT INTO containers (id,name,owner,db) VALUES($1,$2,$3,$4)", resp.ID, name, uid, schemaid)
	if err != nil {
		return types.ContainerJSON{}, err
	}
	return v.Docker.ContainerInspect(context.Background(), resp.ID)
}

func (v *Vafl) DeleteUserDB(u *User, cid string) error {
	err := v.Docker.ContainerStop(context.Background(), cid, nil)
	if err != nil {
		return err
	}

	err = v.Docker.ContainerRemove(context.Background(), cid, types.ContainerRemoveOptions{})
	if err != nil {
		return err
	}

	_, err = v.Database.Exec("DELETE FROM containers WHERE id=$1", cid)
	if err != nil {
		return err
	}
	return nil

}

func (v *Vafl) GetUserDBs(u *User) ([]UserDB, error) {
	rows, err := v.Database.Query(`SELECT containers.id,containers.name,schemas.name
                                 FROM containers
                                 JOIN schemas ON db=schemas.id
                                 WHERE owner = $1
                                   AND NOT containers.id in (SELECT cid FROM testattempt)`, u.Id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var dbs []UserDB
	for rows.Next() {
		var cid string
		var name string
		var sname string
		if err := rows.Scan(&cid, &name, &sname); err != nil {
			return nil, err
		}
		container, err := v.Docker.ContainerInspect(context.Background(), cid)
		if err != nil {
			return nil, err
		}
		dbs = append(dbs, UserDB{
			Name:       name,
			SchemaName: sname,
			Container:  container,
		})

	}
	return dbs, nil
}

func (v *Vafl) GetUserDB(u *User, cid string) (UserDB, error) {
	var name string
	var sname string
	err := v.Database.QueryRow(`SELECT containers.name,schemas.name
                              FROM containers
                              JOIN schemas ON db=schemas.id
                              WHERE owner = $1 AND containers.id = $2`, u.Id, cid).Scan(&name, &sname)
	if err != nil {
		return UserDB{}, err
	}

	container, err := v.Docker.ContainerInspect(context.Background(), cid)
	if err != nil {
		return UserDB{}, err
	}
	return UserDB{
		Name:       name,
		SchemaName: sname,
		Container:  container,
	}, nil
}

// RegisterSchema registers a sql file to be used as a schema
func (v *Vafl) RegisterSchema(name string, path string) error {
	// TODO: Verify path
	_, err := v.Database.Exec("INSERT INTO schemas (name,path) VALUES($1,$2)", name, path)
	return err
}

func (v *Vafl) GetSchemas() ([]DatabaseSchema, error) {
	rows, err := v.Database.Query("SELECT id,name,path FROM schemas")
	if err != nil {
		return nil, err
	}
	var ret []DatabaseSchema
	for rows.Next() {
		var id int
		var name string
		var path string
		if err := rows.Scan(&id, &name, &path); err != nil {
			return nil, err
		}
		ret = append(ret, DatabaseSchema{Id: id, Name: name, Path: path})
	}
	return ret, nil
}

// DBExists checks the database for conflicts
func (v *Vafl) DBExists(uid string, name string) (bool, error) {
	// TODO: Also check docker
	var res bool
	err := v.Database.QueryRow("SELECT count(*) FROM containers WHERE owner=$1 AND name=$2", uid, name).Scan(&res)
	if err != nil {
		return false, err
	}
	return res, nil
}

// QueryDatabase queries a running container and returns the sql results and sql errors if any occurred
// It also logs the executed query.
func (v *Vafl) QueryDatabase(u *User, query string, dbid string, qid, tid int, automated, final bool) (interface{}, error, error) {
	container, err := v.GetUserDB(u, dbid)
	if err != nil {
		return nil, nil, err
	}

	// Establish a new connection to the mysql database
	// TODO: Make this more secure!!
	db, err := sql.Open("mysql", fmt.Sprintf("root:root@tcp(%s:3306)/vafl", container.Container.NetworkSettings.IPAddress))
	if err != nil {
		return nil, nil, err
	}
	defer db.Close()

	// Do not store automated queries
	// Automated queries are used in test mode to provide the correct answers
	if !automated {
		if qid == 0 {
			var queryID int
			err = v.Database.QueryRow(`INSERT INTO queries (query,date,uid,dbid) VALUES
                            ($1,now(),$2,(SELECT schemas.id
                                      FROM containers
                                      JOIN schemas ON db=schemas.id
                                      WHERE containers.id = $3)) RETURNING id`, query, u.Id, dbid).Scan(&queryID)
			if err != nil {
				return nil, nil, err
			}
		} else {
			var queryID int
			err = v.Database.QueryRow(`INSERT INTO queries (query,date,uid,dbid,qid,tid) VALUES
                            ($1,now(),$2,(SELECT schemas.id
                                      FROM containers
                                      JOIN schemas ON db=schemas.id
                                      WHERE containers.id = $3),$4,$5) RETURNING id`, query, u.Id, dbid, qid, tid).Scan(&queryID)
			if err != nil {
				fmt.Println(err)
				return nil, nil, err
			}
			if final {
				_, err := v.Database.Exec(`INSERT INTO finalAnswers (tid,question,uid,query) VALUES ($1,$2,$3,$4)`, tid, qid, u.Id, queryID)
				if err != nil {
					return nil, nil, err
				}
			}
		}
	}

	rows, err := db.Query(query)
	if err != nil {
		return nil, err, nil
	}

	columns, _ := rows.Columns()
	count := len(columns)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)

	var final_result [][]string
	result_id := 0

	// By using reflection we are able to parse the sql result without knowing anything about
	// the queried table
	for rows.Next() {
		for i, _ := range columns {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)

		var tmp_struct []string

		for i, _ := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			tmp_struct = append(tmp_struct, fmt.Sprintf("%s", v))
		}

		final_result = append(final_result, tmp_struct)
		result_id++
	}
	data := struct {
		Columns []string
		Results interface{}
	}{
		Columns: columns,
		Results: final_result,
	}

	return data, nil, nil
}

func (db *UserDB) RefreshER() error {
	home := os.Getenv("VAFL_HOME")
	return exec.Command(home+"/generateDiagram.sh", db.Container.NetworkSettings.IPAddress, db.Container.ID).Run()
}
