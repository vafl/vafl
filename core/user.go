package core

import (
	"gitlab.com/vafl/vafl/utils"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"google.golang.org/api/classroom/v1"
)

type User struct {
	Vafl        *Vafl
	Id          string
	FirstName   string
	LastName    string
	AccessToken string
	srv         *classroom.Service
}

func (u *User) InitializeService() {
	ctx := context.Background()
	client := oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{AccessToken: u.AccessToken}))

	srv, err := classroom.New(client)
	utils.PanicIf(err)

	u.srv = srv
}

func (u *User) InitializeFields() error {
	if u.srv == nil {
		u.InitializeService()
	}
	up, err := u.srv.UserProfiles.Get("me").Do()
	if err != nil {
		return err
	}
	u.FirstName = up.Name.GivenName
	u.LastName = up.Name.FamilyName
	return nil
}

func (u *User) GetTeachingCourses() ([]*classroom.Course, error) {
	res, err := u.srv.Courses.List().TeacherId("me").Do()
	if err != nil {
		return nil, err
	}
	retcourses := []*classroom.Course{}
	for _, c := range res.Courses {
		if c.CourseState == "ACTIVE" {
			retcourses = append(retcourses, c)
		}
	}

	return retcourses, nil
}

func (u *User) GetAvailableTests() ([]TestAttempt, error) {
	res, err := u.Vafl.Database.Query(`SELECT tests.id,tests.name,testattempt.cid
                                       FROM testattempt JOIN tests ON tid=tests.id
                                       WHERE uid=$1
                                         AND tests.end > now()
                                         AND tests.start < now()
                                         AND NOT EXISTS
                                           (SELECT 1 FROM testscore
                                              WHERE testattempt.tid=testscore.tid
                                                AND testscore.sid = testattempt.uid)`, u.Id)
	if err != nil {
		return nil, err
	}
	var tests []TestAttempt
	for res.Next() {
		tst := TestAttempt{}
		err := res.Scan(&tst.Test.Id, &tst.Test.Name, &tst.Container)
		if err != nil {
			return nil, err
		}
		tests = append(tests, tst)
	}
	return tests, nil
}

func (u *User) IsTeacher() bool {
	res, err := u.srv.Courses.List().TeacherId("me").Do()
	if err != nil {
		return false
	}
	if len(res.Courses) == 0 {
		return false
	}
	return true
}

func (u *User) GetCourse(courseID string) (Course, error) {
	c := Course{}

	cres, err := u.srv.Courses.Get(courseID).Do()
	if err != nil {
		return c, err
	}
	c.Name = cres.Name

	sres, err := u.srv.Courses.Students.List(courseID).Do()
	if err != nil {
		return c, err
	}
	var users []User
	for _, s := range sres.Students {
		u := User{}
		u.Id = s.UserId
		u.FirstName = s.Profile.Name.GivenName
		u.LastName = s.Profile.Name.FamilyName
		users = append(users, u)
	}
	c.Students = users

	return c, nil
}
