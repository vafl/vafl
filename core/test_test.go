package core

import (
	"testing"
	"time"
)

func TestCreateTest(t *testing.T) {
	env, err := GetTestEnvironment()
	if err != nil {
		t.Error(err)
		return
	}
	err = env.CreateTest("test-test", 1, time.Now(), time.Now().Add(time.Hour), []string{"42", "43"})
	if err != nil {
		t.Error(err)
	}
	var testPool int
	env.Database.QueryRow("SELECT qpid FROM tests WHERE name=$1", "test-test").Scan(&testPool)
	if testPool != 1 {
		t.Errorf("Wrong pool stored in database! %d != %d", testPool, 1)
	}

	var attemptCount int
	env.Database.QueryRow("SELECT count(*) FROM testattempt").Scan(&attemptCount)
	if attemptCount != 2 {
		t.Error("Not all attempts have been created!")
	}
}
