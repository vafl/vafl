#!/bin/bash
cp example_schema.sql /tmp/schema.sql

docker run --name vafltest -e POSTGRES_PASSWORD=vafl -e POSTGRES_DB=vafl -p 4321:5432 -d postgres
sleep 5
PGPASSWORD=vafl psql -h 127.0.0.1 -d vafl -U postgres -p 4321 < liveschema.sql
go test -v gitlab.com/vafl/vafl/core

docker stop vafltest && docker rm vafltest
containers=('vafl-27219ca6041660624d9da14c6a6c0de6' \
    'vafl-34f7aef845509127797cd009745649b4' \
    'vafl-8798b5e4ef185429de86c127ebe2fd6c' \
    'vafl-ea99c1fedbbeabde0aad9ea4058f8cc8' \
    'vafl-11cbb791f14ab0063d709a48edb094b3' \
    'vafl-b70db5b9de6099acb7f8a74aa2263e5c')
for i in "${containers[@]}"
do
    docker stop $i && docker rm $i
done
