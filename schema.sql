BEGIN;

CREATE TABLE users (
       id TEXT NOT NULL PRIMARY KEY,
       token TEXT NOT NULL
);

CREATE TABLE courses (
       id TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE takesCourse (
       sid TEXT NOT NULL references users(id),
       courseID TEXT NOT NULL references courses(id),
       PRIMARY KEY(sid,courseID)
);

CREATE TABLE databases (
       id SERIAL NOT NULL PRIMARY KEY,
       schema TEXT NOT NULL,
       name TEXT NOT NULL UNIQUE
);


CREATE TABLE questions (
       id SERIAL NOT NULL PRIMARY KEY,
       content TEXT NOT NULL,
       query TEXT NOT NULL,
       points INTEGER NOT NULL,
       tid TEXT NOT NULL references users(id)
);

CREATE TABLE questionpools (
       id SERIAL NOT NULL PRIMARY KEY,
       dbid INTEGER NOT NULL references databases(id),
       name TEXT NOT NULL
);

CREATE TABLE inpool (
       pid INTEGER NOT NULL references questionpools(id) ON DELETE CASCADE,
       qid INTEGER NOT NULL references questions(id) ON DELETE CASCADE,
       PRIMARY KEY(pid,qid)
);

CREATE TABLE tests (
       id SERIAL NOT NULL PRIMARY KEY,
       name TEXT NOT NULL,
       courseID TEXT NOT NULL references courses(id),
       dbid INTEGER NOT NULL references databases(id),
       qpid INTEGER NOT NULL references questionpools(id)
);

CREATE TABLE queries (
       id SERIAL NOT NULL PRIMARY KEY,
       query TEXT NOT NULL,
       date TIMESTAMP NOT NULL,
       uid TEXT NOT NULL references users(id),
       qid INTEGER references questions(id),
       dbid INTEGER NOT NULL references databases(id)
);

CREATE TABLE containers (
       id TEXT NOT NULL PRIMARY KEY,
       name TEXT NOT NULL,
       owner TEXT NOT NULL references users(id),
       db INTEGER NOT NULL references databases(id),
       unique(owner,name)
);

CREATE TABLE testScore (
       date TIMESTAMP NOT NULL,
       sid TEXT references users(id),
       score INTEGER NOT NULL,
       tid INTEGER NOT NULL references tests(id),
       PRIMARY KEY(date,tid,sid)
);

CREATE TABLE testAttempt (
       uid TEXT NOT NULL references users(id) ON DELETE CASCADE,
       tid INTEGER NOT NULL references tests(id) ON DELETE CASCADE,
       cid TEXT NOT NULL references containers(id),
       PRIMARY KEY(uid,tid)
);

CREATE TABLE codebook (
       errorid INTEGER NOT NULL PRIMARY KEY,
       description TEXT NOT NULL
);

END;
