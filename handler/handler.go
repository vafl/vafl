package handler

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/vafl/vafl/core"
	"google.golang.org/api/classroom/v1"
	redis "gopkg.in/redis.v5"
)

type Handler struct {
	Vafl  *core.Vafl
	Redis *redis.Client
}

type Response struct {
	Active      string
	ClientID    string
	CurrentUser *core.User
	Containers  []core.UserDB
	Courses     []*classroom.Course
	Custom      interface{}
}

type EditorResponse struct {
	Name string
	CID  string
}

type Error struct {
	Message string
}

type TestResponse struct {
	Name      string
	CID       string
	TID       int
	Questions []core.Question
}

type ResultResponse struct {
	Test core.Test
}

type QuestionResponse struct {
	Pools []core.Questionpool
}

type CourseResponse struct {
	Course    core.Course
	MetaTests []core.MetaTest
}

func (h *Handler) BuildResponse(u *core.User, template string, c interface{}) Response {
	var dbs []core.UserDB
	var courses []*classroom.Course
	if u != nil {
		dbs, _ = h.Vafl.GetUserDBs(u)
		courses, _ = u.GetTeachingCourses()
	}
	return Response{
		Active:      template,
		ClientID:    h.Vafl.ClientID,
		CurrentUser: u,
		Containers:  dbs,
		Courses:     courses,
		Custom:      c,
	}
}

func (h *Handler) Index(c echo.Context) error {
	u := c.Get("user").(*core.User)
	tests, err := u.GetAvailableTests()
	if err != nil {
		return err
	}
	return c.Render(http.StatusOK, "index", h.BuildResponse(u, "index", tests))
}

func (h *Handler) Editor(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.Param("cid")
	db, err := h.Vafl.GetUserDB(u, cid)
	if err != nil {
		return err
	}
	err = db.RefreshER()
	if err != nil {
		return err
	}
	return c.Render(http.StatusOK, "editor", h.BuildResponse(u, "editor", EditorResponse{Name: db.Name, CID: cid}))
}

func (h *Handler) AuthCallback(c echo.Context) error {
	_, err := c.Cookie("VAFLSESSION")
	if err == nil {
		c.Logger().Warn("Already logged in user called AuthCallback. Deleting old user")
		h.deleteCurrentUser(c)
	}
	err = h.saveCurrentUser(c, c.FormValue("idtkn"), c.FormValue("atkn"))
	if err != nil {
		c.String(http.StatusForbidden, "The user is not a Google Apps user.")
		return nil
	}
	return c.NoContent(http.StatusOK)
}

func (h *Handler) Login(c echo.Context) error {
	if c.QueryParam("logout") != "" {
		h.deleteCurrentUser(c)
	}
	return c.Render(http.StatusOK, "login", h.BuildResponse(nil, "", nil))
}

func (h *Handler) GetDiagram(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.Param("cid")
	db, err := h.Vafl.GetUserDB(u, cid)
	if err != nil {
		return err
	}
	err = db.RefreshER()
	if err != nil {
		return err
	}
	c.Response().Header().Add("Cache-Control", "no-store,no-cache")
	return c.File("/tmp/vafl/" + cid + ".svg")
}

func (h *Handler) GetTest(c echo.Context) error {
	u := c.Get("user").(*core.User)
	tq := c.Param("tid")
	tid, err := strconv.Atoi(tq)
	if err != nil {
		return err
	}
	ta, err := h.Vafl.GetTestAttempt(u, tid)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	if !inTimeSpan(ta.Test.Start, ta.Test.End, time.Now().UTC()) {
		return c.NoContent(http.StatusForbidden)
	}

	return c.Render(http.StatusOK, "test", h.BuildResponse(u, "test", TestResponse{
		Name:      ta.Test.Name,
		CID:       ta.Container,
		TID:       tid,
		Questions: ta.Test.Pool.Questions,
	}))
}

func (h *Handler) GetQuestions(c echo.Context) error {
	u := c.Get("user").(*core.User)
	pools, err := h.Vafl.GetQuestionpools()
	if err != nil {
		return err
	}
	return c.Render(http.StatusOK, "questions", h.BuildResponse(u, "questions", pools))
}

func (h *Handler) GetCourse(c echo.Context) error {
	u := c.Get("user").(*core.User)

	cid := c.Param("cid")
	if cid == "" {
		return c.NoContent(http.StatusBadRequest)
	}

	course, err := u.GetCourse(cid)
	if err != nil {
		c.Logger().Error(err)
		return err
	}

	metatests, err := h.Vafl.GetMetaTests(cid)
	if err != nil {
		c.Logger().Error(err)
		return err
	}

	return c.Render(http.StatusOK, "course", h.BuildResponse(u, "course", CourseResponse{Course: course, MetaTests: metatests}))
}

func (h *Handler) GetResults(c echo.Context) error {
	u := c.Get("user").(*core.User)

	t := c.Param("tid")
	tid, err := strconv.Atoi(t)
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid test id"})
	}

	res, err := h.Vafl.GetTestResults(tid)
	if err != nil {
		c.Logger().Error(err)
		return err
	}

	return c.Render(http.StatusOK, "results", h.BuildResponse(u, "course", res))
}
