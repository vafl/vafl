package handler

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/vafl/vafl/core"
)

func (h *Handler) CreateContainer(c echo.Context) error {
	u := c.Get("user").(*core.User)
	name := c.FormValue("name")
	if name == "" {
		return c.JSON(http.StatusBadRequest, Error{Message: "Name can not be empty"})
	}
	dbid, err := strconv.Atoi(c.FormValue("dbid"))
	_, err = h.Vafl.NewUserDB(u.Id, dbid, name)
	if err != nil {
		c.Logger().Error(err.Error())
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	return c.NoContent(http.StatusOK)
}

func (h *Handler) DeleteContainer(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.Param("cid")
	err := h.Vafl.DeleteUserDB(u, cid)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	return c.NoContent(http.StatusOK)
}

func (h *Handler) GetDatabases(c echo.Context) error {
	dbs, err := h.Vafl.GetSchemas()
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, dbs)
}

func (h *Handler) QueryDatabase(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.FormValue("cid")
	query := c.FormValue("query")
	qids := c.FormValue("qid")
	tids := c.FormValue("tid")
	qid, tid := 0, 0
	qid, _ = strconv.Atoi(qids)
	tid, _ = strconv.Atoi(tids)
	data, mysqlerr, err := h.Vafl.QueryDatabase(u, query, cid, qid, tid, false, false)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	if mysqlerr != nil {
		return c.JSON(http.StatusOK, Error{Message: mysqlerr.Error()})
	}
	return c.JSON(http.StatusOK, data)
}

func (h *Handler) UploadSchema(c echo.Context) error {
	dbname := c.FormValue("dbname")
	if dbname == "" {
		return c.JSON(http.StatusBadRequest, Error{Message: "Database name not specified"})
	}

	file, err := c.FormFile("file")
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}

	src, err := file.Open()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	defer src.Close()

	hash := sha1.New()
	hash.Write([]byte(file.Filename))
	n := hash.Sum(nil)
	path := "/var/vafl/" + hex.EncodeToString(n)

	h.Vafl.RegisterSchema(dbname, path)

	dst, err := os.Create(path)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	defer dst.Close()

	if _, err := io.Copy(dst, src); err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (h *Handler) GetQuestion(c echo.Context) error {
	qraw := c.Param("qid")
	qid, err := strconv.Atoi(qraw)
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid QuestionID"})
	}
	q, err := h.Vafl.GetQuestion(qid)
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, q)
}

func (h *Handler) PutQuestion(c echo.Context) error {
	qraw := c.Param("qid")
	qid, err := strconv.Atoi(qraw)
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid QuestionID"})
	}
	content := c.FormValue("content")
	query := c.FormValue("query")
	praw := c.FormValue("points")
	points, err := strconv.Atoi(praw)
	if err != nil || points < 0 {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid Points"})
	}
	q := core.Question{Id: qid, Query: query, Content: content, Points: points}
	err = h.Vafl.UpdateQuestion(q)
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	return c.NoContent(http.StatusOK)
}

func (h *Handler) DeleteQuestion(c echo.Context) error {
	qraw := c.Param("qid")
	qid, err := strconv.Atoi(qraw)

	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid QuestionID"})
	}
	err = h.Vafl.DeleteQuestion(qid)

	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	return c.NoContent(http.StatusOK)
}

func (h *Handler) AddQuestion(c echo.Context) error {
	u := c.Get("user").(*core.User)
	content := c.FormValue("content")
	query := c.FormValue("query")
	praw := c.FormValue("points")
	pool := c.FormValue("pool")
	points, err := strconv.Atoi(praw)
	if err != nil || points < 0 {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid Points"})
	}
	pid, err := strconv.Atoi(pool)
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid Pool"})
	}
	q := core.Question{Query: query, Content: content, Points: points, User: *u}

	qid, err := h.Vafl.AddQuestion(q, pid)
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, struct{ Id int }{Id: qid})
}

func (h *Handler) GetQuestionpools(c echo.Context) error {
	pools, err := h.Vafl.GetQuestionpools()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	var pls []struct {
		Id   int
		Name string
	}
	for _, v := range pools {
		pls = append(pls, struct {
			Id   int
			Name string
		}{Id: v.Id, Name: v.Name})
	}
	return c.JSON(http.StatusOK, pls)
}

func (h *Handler) AddQuestionpool(c echo.Context) error {
	name := c.FormValue("name")
	schema := c.FormValue("schema")
	sid, err := strconv.Atoi(schema)
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid schema id"})
	}
	if h.Vafl.AddQuestionpool(name, sid) != nil {
		return c.NoContent(http.StatusInternalServerError)
	}
	return c.NoContent(http.StatusCreated)
}

func (h *Handler) PostTest(c echo.Context) error {
	sraw := c.FormValue("students")
	pool, err := strconv.Atoi(c.FormValue("pool"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid pool id"})
	}
	name := c.FormValue("name")
	var students []string
	json.Unmarshal([]byte(sraw), &students)

	start := c.FormValue("start")
	end := c.FormValue("end")

	tstart, err := time.Parse(time.RFC3339, start)

	tend, err := time.Parse(time.RFC3339, end)

	err = h.Vafl.CreateTest(name, pool, tstart, tend, students)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, Error{Message: err.Error()})
	}
	return c.NoContent(http.StatusOK)

}

func (h *Handler) GetExpectedResults(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.Param("cid")
	q := c.QueryParam("qid")
	qid, err := strconv.Atoi(q)
	question, err := h.Vafl.GetQuestion(qid)
	if err != nil {
		return err
	}
	data, mysqlerr, err := h.Vafl.QueryDatabase(u, question.Query, cid, 0, 0, true, false)
	if err != nil {
		return err
	}
	if mysqlerr != nil {
		return c.JSON(http.StatusOK, Error{Message: mysqlerr.Error()})
	}
	return c.JSON(http.StatusOK, data)
}

func (h *Handler) GetTestQuestions(c echo.Context) error {
	u := c.Get("user").(*core.User)
	tq := c.Param("tid")
	tid, err := strconv.Atoi(tq)
	if err != nil {
		return err
	}

	ta, err := h.Vafl.GetTestAttempt(u, tid)
	for idx, _ := range ta.Test.Pool.Questions {
		ta.Test.Pool.Questions[idx].Query = "" // Remove query to prevent cheating
	}
	return c.JSON(http.StatusOK, ta.Test.Pool.Questions)
}

func (h *Handler) CheckQuestion(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.FormValue("cid")
	q := c.Param("qid")
	query := c.FormValue("query")

	qid, err := strconv.Atoi(q)
	if err != nil {
		return err
	}

	question, err := h.Vafl.GetQuestion(qid)
	if err != nil {
		return err
	}

	correct, err := h.answerCorrect(u, query, question.Query, cid, 0, 0, false)
	if err != nil {
		c.Logger().Error(err)
		return err
	}

	if correct {
		return c.NoContent(http.StatusOK)
	} else {
		return c.NoContent(http.StatusNotAcceptable)
	}
}

func (h *Handler) SubmitTest(c echo.Context) error {
	u := c.Get("user").(*core.User)
	cid := c.QueryParam("cid")
	tq := c.Param("tid")
	tid, err := strconv.Atoi(tq)
	if err != nil {
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid Test ID"})
	}

	ta, err := h.Vafl.GetTestAttempt(u, tid)
	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	if !inTimeSpan(ta.Test.Start, ta.Test.End, time.Now().UTC()) {
		return c.NoContent(http.StatusForbidden)
	}

	req := c.Request()
	decoder := json.NewDecoder(req.Body)
	var submissions []struct {
		Id    int
		Query string
	}

	err = decoder.Decode(&submissions)
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, Error{Message: "Invalid Submission Format"})
	}
	defer req.Body.Close()

	points := 0
	for _, s := range submissions {
		question, err := h.Vafl.GetQuestion(s.Id)
		if err != nil {
			return err
		}
		if c, _ := h.answerCorrect(u, s.Query, question.Query, cid, s.Id, tid, true); c {
			points += question.Points
		}
	}
	err = h.Vafl.SubmitTestScore(u, tid, points)
	if err != nil {
		c.Logger().Error(err)
		return c.NoContent(http.StatusInternalServerError)
	}
	err = h.Vafl.DeleteTestAttempt(u, tid)
	if err != nil {
		c.Logger().Error(err)
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.NoContent(http.StatusOK)
}
