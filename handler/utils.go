package handler

import (
	"fmt"
	"gitlab.com/vafl/vafl/core"
	"time"
)

func (h *Handler) answerCorrect(u *core.User, is, should string, cid string, qid, tid int, final bool) (bool, error) {
	isdata, _, err := h.Vafl.QueryDatabase(u, is, cid, qid, tid, false, final)
	if err != nil {
		return false, err
	}
	shoulddata, _, err := h.Vafl.QueryDatabase(u, should, cid, 0, 0, true, false)
	if err != nil {
		return false, err
	}
	return fmt.Sprintf("%#v", isdata) == fmt.Sprintf("%#v", shoulddata), nil
}

func inTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}
