package handler

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/vafl/vafl/core"
	"gitlab.com/vafl/vafl/utils"
)

func sessionID() string {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return ""
	}
	return base64.URLEncoding.EncodeToString(b)
}

func (h *Handler) saveCurrentUser(c echo.Context, idToken string, accessToken string) error {
	c.Logger().Info("User authenticated")

	u, err := h.Vafl.GetUser(accessToken)
	if err != nil {
		c.Logger().Error(err.Error())
		return err
	}

	err = h.Vafl.UpsertUser(u)
	if err != nil {
		c.Logger().Error(err.Error())
		return err
	}

	sid := sessionID()
	sessionCookie := new(http.Cookie)
	sessionCookie.Name = "VAFLSESSION"
	sessionCookie.Value = sid
	sessionCookie.Expires = time.Now().Add(2 * time.Hour)
	c.SetCookie(sessionCookie)

	rkey := fmt.Sprintf("VAFL:SESSION:%s", sid)
	h.Redis.HSet(rkey, "id_token", idToken)
	h.Redis.HSet(rkey, "access_token", accessToken)
	h.Redis.Expire(rkey, 2*time.Hour)
	c.Logger().Info("Saved session")

	return nil
}

func (h *Handler) deleteCurrentUser(c echo.Context) {
	sessionCookie, err := c.Cookie("VAFLSESSION")
	utils.PanicIf(err)
	rkey := fmt.Sprintf("VAFL:SESSION:%s", sessionCookie.Value)
	h.Redis.Del(rkey)
}

// GetCurrentUser retrieves the currently logged in user
// it caches the user in redis
// If the user is not cached it will fetch it from the database and google classroom
func (h *Handler) GetCurrentUser(c echo.Context) (*core.User, error) {
	sessionCookie, err := c.Cookie("VAFLSESSION")
	if err != nil {
		return nil, errors.New("User not authenticated")
	}

	rkey := fmt.Sprintf("VAFL:SESSION:%s", sessionCookie.Value)
	firstname, _ := h.Redis.HGet(rkey, "firstname").Result()
	lastname, _ := h.Redis.HGet(rkey, "lastname").Result()
	uid, _ := h.Redis.HGet(rkey, "id").Result()
	atkn, _ := h.Redis.HGet(rkey, "access_token").Result()

	if firstname == "" || lastname == "" || uid == "" {
		c.Logger().Info("User not cached, requesting classroom API and caching")

		u, err := h.Vafl.GetUser(atkn)
		if err != nil {
			c.Logger().Error(err.Error())
			return nil, err
		}

		// Set the values in redis
		h.Redis.HSet(rkey, "firstname", u.FirstName)
		h.Redis.HSet(rkey, "lastname", u.LastName)
		h.Redis.HSet(rkey, "id", u.Id)
		h.Redis.HSet(rkey, "access_token", u.AccessToken)
		return u, nil
	}

	ru := &core.User{
		Id:          uid,
		FirstName:   firstname,
		LastName:    lastname,
		AccessToken: atkn,
	}
	ru.InitializeService()
	ru.Vafl = h.Vafl
	return ru, nil
}

func (h *Handler) Authorization(redir bool) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			u, err := h.GetCurrentUser(c)
			if err != nil {
				if redir {
					return c.Redirect(http.StatusTemporaryRedirect, "/login")
				}
				return echo.ErrUnauthorized
			}
			c.Set("user", u)
			return next(c)
		}
	}
}
func IsTeacher(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		u := c.Get("user").(*core.User)
		if !u.IsTeacher() {
			return c.NoContent(http.StatusForbidden)
		}
		return next(c)
	}
}
