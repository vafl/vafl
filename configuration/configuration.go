package configuration

import (
	"fmt"
	"strconv"
)

type Configuration struct {
	Application ApplicationConfiguration
	Database    DatabaseConfiguration
	Redis       RedisConfiguration
	Google      GoogleConfiguration
}

type ApplicationConfiguration struct {
	Port    int
	Address string
}

type DatabaseConfiguration struct {
	Host     string
	User     string
	Name     string
	Password string
	Port     int
}

type GoogleConfiguration struct {
	ClientID string `toml:"client_id"`
}

type RedisConfiguration struct {
	Host     string
	Port     int
	Password string
	DB       int
}

func (app *ApplicationConfiguration) ListenInterface() string {
	return fmt.Sprintf("%s:%s", app.Address, strconv.Itoa(app.Port))
}

func (db *DatabaseConfiguration) ConnectionString() string {
	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable port=%d", db.Host, db.User, db.Password, db.Name, db.Port)
}

func (rd *RedisConfiguration) Addr() string {
	return fmt.Sprintf("%s:%s", rd.Host, strconv.Itoa(rd.Port))
}
