-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 06. Feb 2016 um 18:00
-- Server Version: 5.6.21
-- PHP-Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `vafl`
--

CREATE DATABASE vafl;
USE vafl;

--
-- Tabellenstruktur für Tabelle `arbeitet_an`
--

CREATE TABLE IF NOT EXISTS `arbeitet_an` (
  `persnr` int(10) unsigned NOT NULL,
  `projektnr` tinyint(3) unsigned NOT NULL,
  `rolle` varchar(30) CHARACTER SET latin1 COLLATE latin1_german1_ci NOT NULL,
  `stunden` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `arbeitet_an`
--

INSERT INTO `arbeitet_an` (`persnr`, `projektnr`, `rolle`, `stunden`) VALUES
(7566, 20, 'Senior Manager', 40),
(7566, 100, 'Manager', 30),
(7566, 200, 'Manager', 15),
(7902, 100, 'Programmierer', 12),
(7902, 200, 'Programmierer', 120),
(7910, 20, 'Datenbank Admin', 50),
(7910, 100, 'Datenbank Admin', 20),
(8001, 100, 'Junior Programmierer', 210),
(8001, 200, 'Junior Programmierer', 140),
(8002, 200, 'Junior Programmierer', 25);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rolle`
--

CREATE TABLE IF NOT EXISTS `rolle` (
  `rolle` varchar(30) CHARACTER SET latin1 COLLATE latin1_german1_ci NOT NULL,
  `gehalt` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rolle`
--

INSERT INTO `rolle` (`rolle`, `gehalt`) VALUES
('Datenbank Admin', '110.55'),
('Junior Programmierer', '57.60'),
('Manager', '150.23'),
('Programmierer', '98.33'),
('Senior Manager', '190.70');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `arbeitet_an`
--
ALTER TABLE `arbeitet_an`
 ADD PRIMARY KEY (`persnr`,`projektnr`), ADD KEY `rolle` (`rolle`);

--
-- Indizes für die Tabelle `rolle`
--
ALTER TABLE `rolle`
 ADD PRIMARY KEY (`rolle`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
