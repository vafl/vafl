#!/bin/sh
mkdir /tmp/vafl
java -classpath $(echo $SC_HOME/lib/*.jar | tr ' ' ':') schemacrawler.Main -server=mysql -host $1 -database=vafl -user=root -password=root -infolevel=maximum -command=graph -outputformat=svg -outputfile=/tmp/vafl/$2.svg
