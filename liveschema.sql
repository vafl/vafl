--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: codebook; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE codebook (
    errorid integer NOT NULL,
    description text NOT NULL
);


ALTER TABLE codebook OWNER TO postgres;

--
-- Name: containers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE containers (
    id text NOT NULL,
    name text NOT NULL,
    owner text NOT NULL,
    db integer NOT NULL
);


ALTER TABLE containers OWNER TO postgres;

--
-- Name: courses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE courses (
    id text NOT NULL
);


ALTER TABLE courses OWNER TO postgres;

--
-- Name: schemas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE schemas (
    id integer NOT NULL,
    path text NOT NULL,
    name text NOT NULL
);


ALTER TABLE schemas OWNER TO postgres;

--
-- Name: databases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE databases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE databases_id_seq OWNER TO postgres;

--
-- Name: databases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE databases_id_seq OWNED BY schemas.id;


--
-- Name: finalanswers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE finalanswers (
    tid integer NOT NULL,
    question integer NOT NULL,
    uid text NOT NULL,
    query integer NOT NULL
);


ALTER TABLE finalanswers OWNER TO postgres;

--
-- Name: inpool; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inpool (
    pid integer NOT NULL,
    qid integer NOT NULL
);


ALTER TABLE inpool OWNER TO postgres;

--
-- Name: manages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE manages (
    teacher text NOT NULL,
    course text NOT NULL
);


ALTER TABLE manages OWNER TO postgres;

--
-- Name: queries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE queries (
    id integer NOT NULL,
    query text NOT NULL,
    date timestamp without time zone NOT NULL,
    uid text NOT NULL,
    qid integer,
    dbid integer NOT NULL,
    tid integer
);


ALTER TABLE queries OWNER TO postgres;

--
-- Name: queries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE queries_id_seq OWNER TO postgres;

--
-- Name: queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE queries_id_seq OWNED BY queries.id;


--
-- Name: questionpools; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE questionpools (
    id integer NOT NULL,
    name text NOT NULL,
    dbid integer NOT NULL
);


ALTER TABLE questionpools OWNER TO postgres;

--
-- Name: questionpools_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questionpools_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questionpools_id_seq OWNER TO postgres;

--
-- Name: questionpools_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questionpools_id_seq OWNED BY questionpools.id;


--
-- Name: questions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE questions (
    id integer NOT NULL,
    content text NOT NULL,
    query text NOT NULL,
    points integer NOT NULL,
    tid text NOT NULL
);


ALTER TABLE questions OWNER TO postgres;

--
-- Name: questions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questions_id_seq OWNER TO postgres;

--
-- Name: questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questions_id_seq OWNED BY questions.id;


--
-- Name: takescourse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE takescourse (
    sid text NOT NULL,
    courseid text NOT NULL
);


ALTER TABLE takescourse OWNER TO postgres;

--
-- Name: testattempt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE testattempt (
    uid text NOT NULL,
    tid integer NOT NULL,
    cid text NOT NULL
);


ALTER TABLE testattempt OWNER TO postgres;

--
-- Name: tests; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tests (
    id integer NOT NULL,
    name text NOT NULL,
    qpid integer NOT NULL,
    "end" timestamp without time zone NOT NULL,
    start timestamp without time zone NOT NULL
);


ALTER TABLE tests OWNER TO postgres;

--
-- Name: tests_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tests_id_seq OWNER TO postgres;

--
-- Name: tests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tests_id_seq OWNED BY tests.id;


--
-- Name: testscore; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE testscore (
    date timestamp without time zone NOT NULL,
    sid text NOT NULL,
    score integer NOT NULL,
    tid integer NOT NULL
);


ALTER TABLE testscore OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id text NOT NULL,
    token text NOT NULL
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY queries ALTER COLUMN id SET DEFAULT nextval('queries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionpools ALTER COLUMN id SET DEFAULT nextval('questionpools_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions ALTER COLUMN id SET DEFAULT nextval('questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schemas ALTER COLUMN id SET DEFAULT nextval('databases_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tests ALTER COLUMN id SET DEFAULT nextval('tests_id_seq'::regclass);


--
-- Name: codebook_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY codebook
    ADD CONSTRAINT codebook_pkey PRIMARY KEY (errorid);


--
-- Name: containers_owner_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY containers
    ADD CONSTRAINT containers_owner_name_key UNIQUE (owner, name);


--
-- Name: containers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY containers
    ADD CONSTRAINT containers_pkey PRIMARY KEY (id);


--
-- Name: courses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);


--
-- Name: databases_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schemas
    ADD CONSTRAINT databases_name_key UNIQUE (name);


--
-- Name: databases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schemas
    ADD CONSTRAINT databases_pkey PRIMARY KEY (id);


--
-- Name: finalanswers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY finalanswers
    ADD CONSTRAINT finalanswers_pkey PRIMARY KEY (tid, question, uid);


--
-- Name: inpool_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inpool
    ADD CONSTRAINT inpool_pkey PRIMARY KEY (pid, qid);


--
-- Name: queries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY queries
    ADD CONSTRAINT queries_pkey PRIMARY KEY (id);


--
-- Name: questionpools_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionpools
    ADD CONSTRAINT questionpools_pkey PRIMARY KEY (id);


--
-- Name: questions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- Name: takescourse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY takescourse
    ADD CONSTRAINT takescourse_pkey PRIMARY KEY (sid, courseid);


--
-- Name: testattempt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testattempt
    ADD CONSTRAINT testattempt_pkey PRIMARY KEY (uid, tid);


--
-- Name: tests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tests
    ADD CONSTRAINT tests_pkey PRIMARY KEY (id);


--
-- Name: testscore_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testscore
    ADD CONSTRAINT testscore_pkey PRIMARY KEY (date, tid, sid);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: containers_db_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY containers
    ADD CONSTRAINT containers_db_fkey FOREIGN KEY (db) REFERENCES schemas(id);


--
-- Name: containers_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY containers
    ADD CONSTRAINT containers_owner_fkey FOREIGN KEY (owner) REFERENCES users(id);


--
-- Name: finalanswers_query_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY finalanswers
    ADD CONSTRAINT finalanswers_query_fkey FOREIGN KEY (query) REFERENCES queries(id);


--
-- Name: finalanswers_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY finalanswers
    ADD CONSTRAINT finalanswers_question_fkey FOREIGN KEY (question) REFERENCES questions(id);


--
-- Name: finalanswers_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY finalanswers
    ADD CONSTRAINT finalanswers_test_fkey FOREIGN KEY (tid) REFERENCES tests(id);


--
-- Name: finalanswers_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY finalanswers
    ADD CONSTRAINT finalanswers_uid_fkey FOREIGN KEY (uid) REFERENCES users(id);


--
-- Name: inpool_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inpool
    ADD CONSTRAINT inpool_pid_fkey FOREIGN KEY (pid) REFERENCES questionpools(id);


--
-- Name: inpool_qid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inpool
    ADD CONSTRAINT inpool_qid_fkey FOREIGN KEY (qid) REFERENCES questions(id) ON DELETE CASCADE;


--
-- Name: manages_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manages
    ADD CONSTRAINT manages_course_fkey FOREIGN KEY (course) REFERENCES courses(id);


--
-- Name: manages_teacher_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY manages
    ADD CONSTRAINT manages_teacher_fkey FOREIGN KEY (teacher) REFERENCES users(id);


--
-- Name: queries_dbid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY queries
    ADD CONSTRAINT queries_dbid_fkey FOREIGN KEY (dbid) REFERENCES schemas(id);


--
-- Name: queries_qid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY queries
    ADD CONSTRAINT queries_qid_fkey FOREIGN KEY (qid) REFERENCES questions(id);


--
-- Name: queries_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY queries
    ADD CONSTRAINT queries_tid_fkey FOREIGN KEY (tid) REFERENCES tests(id);


--
-- Name: queries_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY queries
    ADD CONSTRAINT queries_uid_fkey FOREIGN KEY (uid) REFERENCES users(id);


--
-- Name: questionpools_dbid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questionpools
    ADD CONSTRAINT questionpools_dbid_fkey FOREIGN KEY (dbid) REFERENCES schemas(id);


--
-- Name: questions_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_tid_fkey FOREIGN KEY (tid) REFERENCES users(id);


--
-- Name: takescourse_courseid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY takescourse
    ADD CONSTRAINT takescourse_courseid_fkey FOREIGN KEY (courseid) REFERENCES courses(id);


--
-- Name: takescourse_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY takescourse
    ADD CONSTRAINT takescourse_sid_fkey FOREIGN KEY (sid) REFERENCES users(id);


--
-- Name: testattempt_cid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testattempt
    ADD CONSTRAINT testattempt_cid_fkey FOREIGN KEY (cid) REFERENCES containers(id);


--
-- Name: testattempt_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testattempt
    ADD CONSTRAINT testattempt_tid_fkey FOREIGN KEY (tid) REFERENCES tests(id) ON DELETE CASCADE;


--
-- Name: testattempt_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testattempt
    ADD CONSTRAINT testattempt_uid_fkey FOREIGN KEY (uid) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: testcore_tid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testscore
    ADD CONSTRAINT testcore_tid_fkey FOREIGN KEY (tid) REFERENCES tests(id) ON DELETE CASCADE;


--
-- Name: tests_qpid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tests
    ADD CONSTRAINT tests_qpid_fkey FOREIGN KEY (qpid) REFERENCES questionpools(id);


--
-- Name: testscore_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY testscore
    ADD CONSTRAINT testscore_sid_fkey FOREIGN KEY (sid) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

