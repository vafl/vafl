package main

import (
	"database/sql"
	"html/template"
	"io"

	"github.com/BurntSushi/toml"
	docker "github.com/docker/docker/client"
	"github.com/eknkc/amber"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	redis "gopkg.in/redis.v5"

	c "gitlab.com/vafl/vafl/configuration"
	"gitlab.com/vafl/vafl/core"
	h "gitlab.com/vafl/vafl/handler"
	"gitlab.com/vafl/vafl/utils"
)

// Echo requires a certain signature to render templates, thats why we need to wrap our map of templates
type RenderWrapper struct {
	templates map[string]*template.Template
}

func (r *RenderWrapper) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return r.templates[name].Execute(w, data)
}

func main() {
	var settings c.Configuration
	_, err := toml.DecodeFile("vafl.toml", &settings) // Parse the configuration file TODO: Use a command parameter for this
	utils.PanicIf(err)

	templates, err := amber.CompileDir("templates", amber.DefaultDirOptions, amber.DefaultOptions) // compile all HTML templates
	utils.PanicIf(err)
	r := &RenderWrapper{templates: templates} // Wrap the compiled templates so that echo can use them

	// connect to the postgres database
	db, err := sql.Open("postgres", settings.Database.ConnectionString())
	utils.PanicIf(err)

	// connect to the redis host
	rcl := redis.NewClient(&redis.Options{
		Addr:     settings.Redis.Addr(),
		Password: settings.Redis.Password,
		DB:       settings.Redis.DB,
	})

	_, err = rcl.Ping().Result() // Test the redis connection
	utils.PanicIf(err)

	// connect to the docker daemon
	dcl, err := docker.NewEnvClient() // TODO: Use settings specified in vafl.toml
	utils.PanicIf(err)

	// store the connections in a common struct
	vafl := core.Vafl{
		Database: db,
		ClientID: settings.Google.ClientID,
		Docker:   dcl,
	}

	// initialize the handler for our endpoints
	hnd := h.Handler{
		Vafl:  &vafl,
		Redis: rcl,
	}

	e := echo.New()

	e.Renderer = r

	// Register static components
	// bower components is for the bower package manager
	// assets stores js, css and images
	e.Static("/bower_components", "bower_components")
	e.Static("/assets", "assets")

	e.Use(middleware.Logger())
	e.Logger.SetLevel(log.DEBUG) // TODO: Use log level from vafl.toml
	e.Use(middleware.Recover())  // Do not crash the server if any goroutine panics

	// ================
	// || API Routes ||
	// ================
	g := e.Group("/api", hnd.Authorization(false)) // Require Authorization but do not redirect to the login page
	g.GET("/databases", hnd.GetDatabases)
	g.POST("/databases/query", hnd.QueryDatabase)
	g.POST("/containers", hnd.CreateContainer)
	g.DELETE("/containers/:cid", hnd.DeleteContainer)
	g.POST("/upload", hnd.UploadSchema)
	g.GET("/questions/:qid", hnd.GetQuestion, h.IsTeacher)
	g.PUT("/questions/:qid", hnd.PutQuestion, h.IsTeacher)
	g.DELETE("/questions/:qid", hnd.DeleteQuestion, h.IsTeacher)
	g.POST("/questions", hnd.AddQuestion, h.IsTeacher)
	g.GET("/questionpools", hnd.GetQuestionpools, h.IsTeacher)
	g.POST("/questionpools", hnd.AddQuestionpool, h.IsTeacher)
	g.POST("/tests", hnd.PostTest, h.IsTeacher)
	g.GET("/expectedresults/:cid", hnd.GetExpectedResults, h.IsTeacher)
	g.GET("/testquestions/:tid", hnd.GetTestQuestions)
	g.POST("/checkquestion/:qid", hnd.CheckQuestion)
	g.POST("/tests/:tid/scores", hnd.SubmitTest)

	// ================
	// || Web Routes ||
	// ================
	e.GET("/login", hnd.Login)
	e.POST("/auth", hnd.AuthCallback)
	e.GET("/", hnd.Index, hnd.Authorization(true)) // Redirect to the login page if not authorized
	e.GET("/editor/:cid", hnd.Editor, hnd.Authorization(true))
	e.GET("/diagram/:cid", hnd.GetDiagram, hnd.Authorization(true))
	e.GET("/test/:tid", hnd.GetTest, hnd.Authorization(true))
	e.GET("/questions", hnd.GetQuestions, hnd.Authorization(true), h.IsTeacher)
	e.GET("/courses/:cid", hnd.GetCourse, hnd.Authorization(true), h.IsTeacher)
	e.GET("/results/:tid", hnd.GetResults, hnd.Authorization(true), h.IsTeacher)

	// We are live!
	e.Start(settings.Application.ListenInterface())
}
